package com.bosssoft.permission.centre.util;

/**
 * <p>RabbitMQ 常量</p>
 * @author 周靖赟
 * @version 1.0
 * @since  12/10/20
 */
public class MqConstant {
    public static final String EXCHANGE_NAME = "directs";
    public static final String DICTIONARY_TYPE = "dictionary_type";
}
