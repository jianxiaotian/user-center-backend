/*
 Navicat Premium Data Transfer

 Source Server         : 172.20.3.94
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 172.20.3.94:3306
 Source Schema         : train_usercentre

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 22/07/2022 08:22:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for basic_job_manage
-- ----------------------------
DROP TABLE IF EXISTS `basic_job_manage`;
CREATE TABLE `basic_job_manage`  (
  `id` bigint(20) NOT NULL,
  `job_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务分组',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务描述',
  `target_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Cron执行表达式',
  `error_tactics` tinyint(4) DEFAULT 1 COMMENT '计划错误策略,1错过计划等待本次计划完成后立即执行一次,0本次执行时间根据上次结束时间重新计算（时间间隔方式）',
  `concurrent_state` tinyint(4) DEFAULT 1 COMMENT '是否执行并发,1为执行，0为不执行',
  `other_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '其他信息',
  `tenant_id` bigint(20) NOT NULL COMMENT '租户id',
  `created_by` bigint(20) DEFAULT NULL COMMENT '创建人 0用户',
  `creator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_time` datetime(0) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL COMMENT '修改人',
  `modifier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `updated_time` datetime(0) DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '状态值，1为启用，0为停用',
  `version` bigint(20) NOT NULL,
  `org_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
