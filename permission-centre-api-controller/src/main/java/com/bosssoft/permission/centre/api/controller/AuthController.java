/**
 * Copyright (C), 2001-2024, www.bosssof.com.cn
 * FileName: AuthController
 * Author: abel.zhan
 * Date: 2024-01-19 15:56:41
 * Description:
 * <p>
 * History:
 * <author> abel.zhan
 * <time> 2024-01-19 15:56:41
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.bosssoft.permission.centre.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: AuthController
 * @Description: 类的主要功能描述
 * @Author: abel.zhan
 * @Date: 2024-01-19 15:56
 **/
@RestController
@RequestMapping("permission/center/v1/user")
public class AuthController {
    @GetMapping("")
    public String hello(){
        return "hello everyone !";
    }
}
