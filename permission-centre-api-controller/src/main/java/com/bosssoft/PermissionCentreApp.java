package com.bosssoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Class PermissionCentreApp
 * @Description
 * abel.zhan 2023-05-19 禁止 nacos命名logback
 * @Author Administrator
 * @Date 2023-05-19  10:09
 * @version 1.0.0
 */
@SpringBootApplication
//@ComponentScans({
//        @ComponentScan("com.bosssoft.common"),
//        @ComponentScan("com.bosssoft.common.api.swagger")
//})
@EnableSwagger2

public class PermissionCentreApp {
    public static void main( String[] args )
    {
        SpringApplication.run(PermissionCentreApp.class, args);
    }
}
